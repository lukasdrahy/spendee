### Howto

1. Change cache adapter in config/packages/cache.yaml :

```yaml
framework:
    cache:
        app: cache.adapter.filesystem
        # You can switch to redis with default config here
        #app: cache.adapter.redis
        system: cache.adapter.system
```

2. Change data source in config/services.yaml:

```yaml
services:
    App\Repository\WatchRepositoryWrapper:
        arguments:
            - '@cache.app'
            - '@App\Repository\MysqlWatchRepository'
            # Or use XmlWatchRepository
            # - '@App\Repository\XmlWatchRepository'
```

3. Then :
```
bin/console cache:clear
symfony server:start
vendor/bin/codecept run
```

4. You can acces endpoint with browser http://localhost:8000/watch/1