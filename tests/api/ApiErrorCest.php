<?php namespace App\Tests;
use App\Tests\ApiTester;

class ApiErrorCest
{
    public function _before(ApiTester $I)
    {
    }

    public function notFoundTest(ApiTester $I)
    {
        $I->sendGET('/not-existing-route');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::NOT_FOUND);
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":"No route found for \"GET \/not-existing-route\""}');
    }
}
