<?php namespace App\Tests;
use App\Tests\ApiTester;

class ApiWatchCest
{
    public function _before(ApiTester $I)
    {
    }

    public function getTestWithInvalidId(ApiTester $I)
    {
        $I->sendGET('/watch/0');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":"Invalid ID \'0\'"}');
   }

    public function getInvalidMethodTest(ApiTester $I)
    {
        $I->sendPOST('/watch/1');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::METHOD_NOT_ALLOWED);
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":"No route found for \"POST \/watch\/1\": Method Not Allowed (Allow: GET)"}');
    }

    public function getTest(ApiTester $I)
    {
        $I->sendGET('/watch/1');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"identification":1,"title":"Watch with water fountain","price":"200","description":"Beautifully crafted timepiece for every gentleman"}');
    }

    public function getSecondTest(ApiTester $I)
    {
        $I->sendGET('/watch/2');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"identification":2,"title":"Watch before water fountain","price":"100","description":"Noting you want to have"}' );
    }

    public function notFoundTest(ApiTester $I)
    {
        $I->sendGET('/watch/3');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::NOT_FOUND);
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":"Watch witth ID \'3\' not found"}' );
    }
}
