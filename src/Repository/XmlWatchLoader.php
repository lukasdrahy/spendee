<?php
/*
 * @copyright   2020 Mautic, Inc. All rights reserved
 * @author      Mautic, Inc.
 *
 * @link        https://mautic.com
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace App\Repository;


use App\Service\DataSource;

class XmlWatchLoader
{
    /**
     * @var DataSource
     */
    private $dataSource;

    public function __construct(DataSource $dataSource)
    {
        $this->dataSource = $dataSource;
    }

    public function loadByIdFromXml(int $watchIdentification): ?array
    {
        return $this->dataSource->getById((int) $watchIdentification);
    }
}