<?php

namespace App\Repository;


use App\DTO\WatchDTO;

/**
 * Provides consistent interface in the top of different sources
 * Disagreed "you SHOULD NOT implement the interfaces". Created by myself
 */
interface WatchRepositoryInterface
{
    public function getWatchById(int $id): WatchDTO;
}