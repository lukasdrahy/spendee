<?php
/*
 * @copyright   2020 Mautic, Inc. All rights reserved
 * @author      Mautic, Inc.
 *
 * @link        https://mautic.com
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace App\Repository;


use App\DTO\WatchDTO;

/**
 * Myself created wrapper in the top of xml loader to keep the same interface
 */
class XmlWatchRepository implements WatchRepositoryInterface
{
    /**
     * @var XmlWatchLoader
     */
    private $loader;

    public function __construct(XmlWatchLoader $loader)
    {
        $this->loader = $loader;
    }

    public function getWatchById(int $id): WatchDTO
    {
        return WatchDTO::create($this->loader->loadByIdFromXml($id));
    }
}