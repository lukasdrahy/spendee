<?php
/*
 * @copyright   2020 Mautic, Inc. All rights reserved
 * @author      Mautic, Inc.
 *
 * @link        https://mautic.com
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace App\Repository;


use Symfony\Component\Cache\Adapter\AdapterInterface as Cache;
use Symfony\Contracts\Cache\ItemInterface;

class WatchRepositoryWrapper
{
    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var WatchRepositoryInterface
     */
    private $repository;

    public function __construct(Cache $cache, WatchRepositoryInterface $repository)
    {
        $this->cache = $cache;
        $this->repository = $repository;
    }

    public function fetchWatchById(int $id): array
    {
        $watch = $this->cache->get('watch_'.$id, function (ItemInterface $item) use ($id) {

            $watch = $this->repository->getWatchById($id)->toArray();

            $item->expiresAfter(null);

            return $watch;
        });

        return $watch;
    }
}