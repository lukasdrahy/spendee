<?php

namespace App\Repository;


use App\DTO\WatchDTO;
use App\Service\DataSource;

class MysqlWatchRepository implements WatchRepositoryInterface
{
    /**
     * @var DataSource
     */
    private $dataSource;

    public function __construct(DataSource $dataSource)
    {
        $this->dataSource = $dataSource;
    }

    public function getWatchById(int $id): WatchDTO
    {
        return WatchDTO::create($this->dataSource->getById(($id)));
    }
}