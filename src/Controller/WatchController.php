<?php
/*
 * @copyright   2020 Mautic, Inc. All rights reserved
 * @author      Mautic, Inc.
 *
 * @link        https://mautic.com
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace App\Controller;


use App\Exception\WatchNotFoundException;
use App\Repository\WatchRepositoryWrapper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class WatchController
{
    /**
     * @Route("/watch/{id}", name="app_api_watch_get", methods={"GET"})
     */
    public function get(int $id, WatchRepositoryWrapper $repo): JsonResponse
    {
        if ($id < 1) {
            throw new BadRequestHttpException("Invalid ID '$id'");
        }

        try {
            $entity = $repo->fetchWatchById($id);
        } catch (WatchNotFoundException $e) {
            throw new NotFoundHttpException("Watch witth ID '$id' not found");
        }

        return new JsonResponse($entity);
    }

}