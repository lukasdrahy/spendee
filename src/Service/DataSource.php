<?php
/*
 * @copyright   2020 Mautic, Inc. All rights reserved
 * @author      Mautic, Inc.
 *
 * @link        https://mautic.com
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace App\Service;

use App\Exception\WatchNotFoundException;

/**
 * Faked data resource used in both sources
 */
class DataSource
{
    private $data = [
        1 => [
            'identification' => 1,
            'title' => 'Watch with water fountain',
            'price' => 200,
            'description' => 'Beautifully crafted timepiece for every gentleman',
        ],
        2 => [
            'identification' => 2,
            'title' => 'Watch before water fountain',
            'price' => 100,
            'description' => 'Noting you want to have',
            'noise' => 'some noise'
        ],
    ];

    public function getById(int $id): array
    {
        sleep(1);

        $watch = !empty($this->data[$id]) ? $this->data[$id] : null;

        if (!$watch) {
            throw new WatchNotFoundException();
        }

        return $watch;
    }
}