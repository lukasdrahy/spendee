<?php

namespace App\DTO;


class WatchDTO
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $price;

    /**
     * @var string
     */
    private $description;

    /**
     * @param int $id
     * @param string $title
     * @param string $price
     * @param string $description
     */
    public function __construct(
        int $id,
        string $title,
        string $price,
        string $description
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
        $this->description = $description;
    }

    public static function create(array $data): WatchDTO
    {
        return new self(
            $data['identification'],
            $data['title'],
            $data['price'],
            $data['description']
        );
    }

    /**
     * @todo this should be some serialization component in separated service
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'identification' => $this->id,
            'title'=> $this->title,
            'price' => $this->price,
            'description' => $this->description,
        ];
    }
}